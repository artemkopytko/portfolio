$( document ).ready(function() {
    $('.menu-btn').on('click', function(e) {
        e.preventDefault();
        $(this).toggleClass('menu-btn_active');
        $('.menu-nav').toggleClass('menu-nav_active');
    });

    $('.envelope').on('click', function(e) {
        $('.envelope__top').toggleClass('envelope__top_close');
        $('.paper').toggleClass('paper_close');
    })

    var card = $(".card");
    var cursorHeight = 0;
    $(card).on("mousemove",function(e) {
        var ax = -($(window).innerWidth()/2- e.pageX)/20;
        cursorHeight = (e.pageY % $(window).innerHeight());
        var ay = ($(window).innerHeight()/2- cursorHeight)/10;
        console.log(e.pageY, $(window).innerHeight());
        // console.log(ax,ay);
        if (ay < 0)
            ay = -ay;
        $(this).attr("style", "transform: rotateY("+ax+"deg) rotateX("+ay+"deg);-webkit-transform: rotateY("+ax+"deg) rotateX("+ay+"deg);-moz-transform: rotateY("+ax+"deg) rotateX("+ay+"deg)");

    });

    $(card).on("mouseleave", function (e) {
        // resetCards();
        setTimeout(function () {
            card.each(function () {
                card.attr("style", "transform: rotateY("+0+"deg) rotateX("+0+"deg);-webkit-transform: rotateY("+0+"deg) rotateX("+0+"deg);-moz-transform: rotateY("+0+"deg) rotateX("+0+"deg)");
            })
        }, 1000);

    })
});
